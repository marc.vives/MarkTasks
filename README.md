# MarkTasks 
[web a Gitlab](https://marc.vives.gitlab.io/MarkTasks/list.html)
[web a Heroku](https://marktasks.herokuapp.com/list.html)

## Introducció  
Com a part del projecte del M13 de DAM, volem crear una interfície web que ens permeti gestionar una o més llistes de tasques, les principals accions a realitzar amb els elements de la llista seran: marcar com a fet, afegir una tasca pendent, esborrar una tasca (feta o pendent).


### Nom del projecte  
Partint de la funcionalitat que es pretén cobrir i fent una cerca d’aplicacions similars, s'ha decidit el nom de __MarkTasks__ amb l'ús de la paraula _Tasks_ com a representant dels elements de la llista i el verb _Mark_ com acció principal a realitzar dins l'aplicació web.


## Aparença i disseny  
 ### Imatges  
 <img src="public/img/marktasks_white.png" alt="large logo" width="200"/>
<br>
 <img src="public/img/marktasks_small.png" alt="square logo" width="50"/>


 ### Selecció de colors  
[https://color.adobe.com/create](https://color.adobe.com/create)

 ### Tipografia  
TODO: seleccionar  
recurs: https://fonts.google.com/


## Webgrafia i inspiració  
- https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/App_structure
- https://www.w3schools.com/howto/howto_js_todolist.asp
- https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/App_structure
- https://getbootstrap.com/docs/4.6/getting-started/introduction/#starter-template
- https://to-do.live.com/tasks/


## Histories d'Usuari
### HU01 - Llistat ítems todolist
__Com a:__ Usuari de l’aplicació via web sense autenticar  
__Vull:__ Veure un llistat dels ítems de la meva todolist  
__Per a:__ No oblidar què he de fer o comprar, etc  

### HU02 - Afegir ítems todolist
__Com a:__ Usuari de l’aplicació via web sense autenticar  
__Vull:__ Afegir ítems a la meva todolist  
__Per a:__ Actualitzar la meva todolist i tenir centralitzats els ítems relacionats  
__Criteris d’acceptació - DoD:__  
- Poder escriure el nou ítem en una caixa de text de la web, donar-li a un botó afegir i la llista de todo s’actualitza amb el nou ítem.
- Es veu en la mateixa pàgina la llista actualitz

### HU03 - Marcar/desmarcar ítems todolist
__Com a:__ Usuari de l’aplicació via web sense autenticar  
__Vull:__ Marcar i desmarcar l’ítem de la todolist com fet  
__Per a:__ Veure progrés a través de les feines fetes o no repetir feines  
__Criteris d’acceptació - DoD:__  
-Cada ítem de la todolist hauria de tenir un check (o algun mecanisme visual) per  marcar i desmarcar  


## Bugs:
### Pending:
_empty_

### Solved:
  - bug01 HU3/4  Els elements afegits no es poden marcar com resolts
