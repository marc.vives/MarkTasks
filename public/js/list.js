//TODO:
function getItems() {
    console.log("getItems()");
    var endpoint_list = 'https://tolistoapi.herokuapp.com/tasks';

     fetch(endpoint_list)
          .then(response => response.json())
          .then(data => console.log(data));
    return '[{"id":1,"description":"Manzanas","done":true,"position":0},{"id":2,"description":"Naranjas","done":true,"position":1},{"id":3,"description":"Yogurt","done":false,"position":2}]';
}

function renderItems() {
    var items = JSON.parse('[{"id":1,"description":"Manzanas","done":false,"position":0},{"id":2,"description":"Naranjas","done":true,"position":1},{"id":3,"description":"Yogurt","done":false,"position":2}]');
    for (i = 0; i < items.length; i++) {
        renderItem(items[i]);
    }
}

function renderItem(element) {
    var t = document.createTextNode(element.description);

    var li = document.createElement("li");
    li.classList.add('list-group-item');
    li.appendChild(t);

    document.getElementById("llista").appendChild(li);

    if (element.done) {
        console.log(element.description + " DONE")
        li.classList.add('task-done')
    }

    li.addEventListener('click', function () {
        this.classList.toggle('task-done');
    });

}



//run when the DOM is ready
$(function () {
    renderItems();

    const element = document.querySelector('form');
    element.addEventListener('submit', event => {
        event.preventDefault();
        var inputValue = document.getElementById("taskInput").value;

        if (inputValue === '') {
            alert("Escriu una tasca");
        } else {
            renderItem(new Element(inputValue, false))
        }
        document.getElementById("taskInput").value = "";
    });

});


class Element {
    constructor(description, done) {
        this.description = description;
        this.done = done;
    }
}
